<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class DashBoardController extends Controller
{
    public function home(Request $request){
        $userArr = User::orderBy('id', 'DESC');
        if (isset($request->name)) {
        $userArr =$userArr->Where('name', 'like', '%' .$request->name . '%');
        }

        $userArr=$userArr->get();
        return view('dashboard',compact('userArr'));
    }



    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }
}
