<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

use Validator;

class UserController extends Controller
{

    public function store(Request $request){
        $rules = [
			'first_name' => 'required|string|min:3|max:255',
			'last_name' => 'required|string|min:3|max:255',
            'email' => 'required|string|email:rfc,dns|max:255',
            'phone' => 'required|digits:10',
            'password' => 'required|min:6|max:16',
            'repeat_password' => 'required|same:password'
		];
		$validator = Validator::make($request->all(),$rules);
		if ($validator->fails()) {
			return back()
			->withInput()
			->withErrors($validator);
		}
		else{
            $data = $request->input();
			try{
				$user = new User;
                $user->name = $data['first_name'].' '.$data['last_name'];
                $user->password = bcrypt($data['password']);
                $user->email = $data['email'];
                $user->phone = $data['phone'];
                $user->address = $data['address'];
                $user->save();

                $request->session()->flash('msg','Data submitted');
				return redirect()->route('login');
			}
			catch(\Exception $e){
				return redirect()->route('login')->with('failed',"operation failed");
			}
		}
    }


    public function login()
    {
    	return view('login');
    }


    public function userLogin(Request $request)
    {

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->route('home');
        }

        return back()->withErrors([
            'email' => 'Please enter a valid Email Address.',
            'password' => ' Sorry!! You Entered Wrong Password.'
        ]);
    }




    public function register()
    {
    	return view('register');
    }

    // public function forgot_password()
    // {
    // 	return view('Admin.forgot_password');
    // }


    public function getuser(Request $request){

        $userArr = User::orderBy('id', 'DESC');
        if (isset($request->name)) {
        $userArr =$userArr->Where('name', 'like', '%' .$request->name . '%');
        }

        $userArr=$userArr->get();
        return view('dashboard',compact('userArr'));
    }


    public function user_edit($id)
    {
        $User = User::find($id);

        return view('userEdit',compact('User'));

    }

public function user_update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|string|email:rfc,dns|max:255',
            'phone' => 'required|digits:10',
        ]);
        $User = User::find($id);
        $User->update([
            'name'    => $request->name,
            'email'    => $request->email,
            'phone' => $request->phone,
            'address'    => $request->address,

         ]);

        return redirect()->route('home');


    }
    public function user_destroy($id)
    {
        $User = User::destroy($id);

        return redirect()->route('home');
    }

}


