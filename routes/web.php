<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashBoardController;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if (Auth::check()) {
    	return redirect()->route('home');
    }
    return view('login');
});

Route::namespace('App\Http\Controllers')->group(function(){
Route::group(['middleware' => 'auth'], function() {
    Route::get('logout',[DashBoardController::class,'logout'])->name('logout');
Route::group(['middleware' => 'admin'], function() {
    Route::get('home',[DashBoardController::class,'home'])->name('home');
    Route::get('user_destroy/{id}', [UserController::class,'user_destroy'])->name('user_destroy');
    Route::get('user_edit/{id}', [UserController::class,'user_edit'])->name('user_edit');
    Route::put('user_update/{id}', [UserController::class,'user_update'])->name('user_update');


});
    });

Route::group(['middleware' => 'guest'], function() {
    Route::get('login',[UserController::class,'login'])->name('login');
    Route::post('userLogin',[UserController::class,'userLogin'])->name('userLogin');
    Route::get('/register', [UserController::class , 'register'])->name('register');
    // Route::get('/forgot_password', 'AuthController@forgot_password')->name('forgot_password');
    Route::post('/store', 'UserController@store')->name('store');

});
});


