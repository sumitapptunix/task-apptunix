<div class="table-responsive">

    <table border="2px solid" style="text-align: center" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr>

          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Address</th>

        </tr>
      </thead>
      <tfoot>
        <tr>
          <tr>

            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>

          </tr>
      </tfoot>
      <tbody>

        <tr>

            @foreach ($userArr as $userArr)

            <tr>

            <td>{{ $userArr->name }}</td>
            <td>{{ $userArr->email }}</td>
            <td>{{ $userArr->phone }}</td>
            <td>{{ $userArr->address }}</td>

            <td><a href="{{route('user_edit', $userArr->id)}}" data-toggle=tooltip title="Edit">Edit<i class="fa fa-edit" aria-hidden="true"></i></a>
                <a href="{{route('user_destroy', $userArr->id)}}" data-toggle=tooltip title="Delete">Delete<i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            @endforeach
        </tr>
      </tbody>
    </table>
  </div>
  <br>
  <hr>
  <a class="small" href="{{route('logout')}}">Logout</a>
