<div class="container">

    <form action="{{route('user_update', $User->id)}}" method="POST">
        @method('put')
        @csrf
        <label for="Name">Name:</label>
        <input type="text" name="name" value="{{$User->name}}" required ><br>
        <label for="Email">Email:</label>
        <input type="email" name="email" value="{{$User->email}}" required ><br>
        <label for="phone">Phone:</label>
        <input type="number" name="phone" value="{{$User->phone}}" required >

        @error('phone')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <label for="address">Address:</label>
        <input type="text" name="address" value="{{$User->address}}" required ><br>

        <input type="submit" name="update">
    </form>
</div>
        <!-- End of Main Content -->

